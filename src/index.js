import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";

import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import authReducer from "./store/reducers/auth";
import { fetchProfile } from "./store/actions/fetchProfile";
import { isLoggedInUserExpired } from "./shared/utility";
import { logout } from "./store/actions";

const composeEnhancers =
  process.env.NODE_ENV === "development"
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : null || compose;

const rootReducer = combineReducers({
  auth: authReducer,
});

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);

const isLogin = localStorage.getItem("token");
if( isLogin ){
  // Dispatch logout if JWT token expired
  const expirationTime = isLogin ? isLoggedInUserExpired(isLogin) : "";
  let currentTime = Math.round(new Date().getTime() / 1000);
  if( currentTime > expirationTime ){
    store.dispatch(logout());
  }
  //Dispatch the fetchProfile() before our root component renders
  store.dispatch(fetchProfile());
}

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(
  <React.StrictMode>{app}</React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
