import axios from "axios";

import * as actionTypes from "./actionTypes";

import { isLoggedInUserExpired } from "../../shared/utility";

export const authStart = () => {
  return {
    type: actionTypes.AUTH_START,
  };
};

export const authSuccess = (payload) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    payload: payload,
  };
};

export const authFail = (error) => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: error,
  };
};

export const logout = () => {
  localStorage.removeItem("token");
  
  return (dispatch) =>
    dispatch({
      type: actionTypes.AUTH_LOGOUT,
    });
};

/*
export const checkAuthTimeout = (expirationTime) => {
  return (dispatch) => {
    setTimeout(() => {
      dispatch(logout());
    }, expirationTime * 1000);
  };
};
*/

export const auth = (username, password) => {
  return async (dispatch) => {
    dispatch(authStart());
    const authData = {
      username: username,
      password: password,
      returnSecureToken: true,
    };
    const headers = {
      "Content-Type": "application/json",
    };
    const url = "http://localhost:8080/login";
    try {
      const response = await axios.post(url, authData, headers);
      localStorage.setItem("token", response.data.jwt);
      dispatch(authSuccess(response.data));
    } catch (err) {
      console.log("ERROR", err.response);
      if (err && err.response && err.response.data) {
        dispatch(authFail(err.response.data.message));
      } else {
        dispatch(authFail("SOMETHING WENT WRONG!"));
      }
    }
  };
};

export const setAuthRedirectPath = (path) => {
  return {
    type: actionTypes.SET_AUTH_REDIRECT_PATH,
    path: path,
  };
};

export const authCheckState = () => {
  return (dispatch) => {
    const token = localStorage.getItem("token");
    if (!token) {
      dispatch(logout());
    } else {
      let currentTime = Math.round(new Date().getTime() / 1000);
      const expirationTime = isLoggedInUserExpired(token);
      if (currentTime > expirationTime) {
        dispatch(logout());
      } else {
        const userId = localStorage.getItem("userId");
        dispatch(authSuccess(token, userId));
      }
    }
  };
};

