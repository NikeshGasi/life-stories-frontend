import axios from 'axios';
import * as actionTypes from "./actionTypes";

export const fetchProfileStart = () => {
    return {
      type: actionTypes.FETCH_PROFILE_START,
    };
  };
  
export const fetchProfileSuccess = (payload) => {
  return {
    type: actionTypes.FETCH_PROFILE_SUCCESS,
    payload: payload,
  };
};

export const fetchProfileFail = (error) => {
  return {
    type: actionTypes.FETCH_PROFILE_FAIL,
    error: error,
  };
};

export const fetchProfile = () => {
  return async (dispatch) => {
    dispatch(fetchProfileStart());
    const headers = {
      "Content-Type": "application/json",
      "authorization": `Bearer ${localStorage.getItem("token")}`,
    };
    const url = "http://localhost:8080/profile/";
    try {
      const response = await axios.get(url, {headers});
      localStorage.setItem("token", response.data.jwt);
      dispatch(fetchProfileSuccess(response.data));
    } catch (err) {

      if(err.response.data.status === 500){
        console.log("ERROR STATUS", err.response.data.status);
        localStorage.removeItem("token");
      }

      if (err && err.response && err.response.data) {
        dispatch(fetchProfileFail(err.response.data.message));
      } else {
        dispatch(fetchProfileFail("SOMETHING WENT WRONG!"));
      }
    }
  };
};