import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../shared/utility";

const initialState = {
  userId: null,
  username: null,
  userRole: null,
  error: null,
  loading: false,
  authRedirectPath: "/",
};

const authStart = (state, action) => {
  return updateObject(state, { error: null, loading: true });
};

const authSuccess = (state, action) => {
  return updateObject(state, {
    userId: action.payload.userId,
    username: action.payload.username,
    userRole: action.payload.userRole,
    error: null,
    loading: false,
  });
};

const authFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};

const authLogout = (state, action) => {
  return updateObject(state, { userId : null, username: null, userRole: null });
};

const setAuthRedirectPath = (state, action) => {
  return updateObject(state, { authRedirectPath: action.path });
};

const fetchProfileStart = (state, action) => {
  return updateObject(state, { error: null, loading: false });
};

const fetchProfileSuccess = (state, action) => {
  return updateObject(state, {
    userId: action.payload.userId,
    username: action.payload.username,
    userRole: action.payload.userRole,
    error: null,
    loading: false,
    authRedirectPath: action.payload.userRole === "ADMIN" ? "/admin" : "/"
  });
};

const fetchProfileFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_START:
      return authStart(state, action);
    case actionTypes.AUTH_SUCCESS:
      return authSuccess(state, action);
    case actionTypes.AUTH_FAIL:
      return authFail(state, action);
    case actionTypes.AUTH_LOGOUT:
      return authLogout(state, action);
    case actionTypes.SET_AUTH_REDIRECT_PATH:
      return setAuthRedirectPath(state, action);
    case actionTypes.FETCH_PROFILE_START:
      return fetchProfileStart(state, action);
    case actionTypes.FETCH_PROFILE_SUCCESS:
      return fetchProfileSuccess(state, action);
    case actionTypes.FETCH_PROFILE_FAIL:
      return fetchProfileFail(state, action);
    default:
      return state;
  }
};

export default reducer;