import React from "react";
import { Route, Redirect } from "react-router-dom";

import { loggedInUserRole } from "../shared/utility";

const AuthRoute = ({ component: Component, ...rest }) => {
  const isLogin = localStorage.getItem("token");
  const isAdmin = isLogin ? loggedInUserRole(isLogin) : "";

  return (
    // Show the component only when the user is logged in
    // Otherwise, redirect the user to /signin page
    <Route
      {...rest}
      render={(props) =>
        isAdmin !== "ADMIN" ? <Component {...props} /> : <Redirect to="/admin" />
      }
    />
  );
};

export default AuthRoute;
