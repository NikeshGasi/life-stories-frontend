import React from "react";
import { Route, Redirect } from "react-router-dom";
import { loggedInUserRole } from "../shared/utility";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const jwt = localStorage.getItem("token");
  const isAdmin = jwt ? loggedInUserRole(jwt) : "";

  return (
    // Show the component only when the user is logged in
    // Otherwise, redirect the user to /signin page
    <Route
      {...rest}
      render={(props) =>
        isAdmin === "ADMIN" ? <Component {...props} /> : <Redirect to="/" />
      }
    />
  );
};

export default PrivateRoute;
