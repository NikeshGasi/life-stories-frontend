import React from 'react';

const ResetMessage = () => {
    return ( <h2>Please check your email for reset password link.</h2> );
}
 
export default ResetMessage;