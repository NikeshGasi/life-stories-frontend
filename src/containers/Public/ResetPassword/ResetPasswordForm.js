import React, { useState, useEffect } from 'react';
import axios from "axios";

import "./ResetPassword.css";
import Button from "../../../components/UI/Button/Button";
import Spinner from "../../../components/UI/Spinner/Spinner";

const ResetPassword = (props) => {
    const [loading, setLoading] = useState(true);
    const [token, setToken] = useState("");

    const hasToken = ( url ) => {
        if(!url.searchParams.has("token")){
            props.history.push("/");
        } else {
            const token = url.searchParams.get("token");
            setToken(token);
            setLoading(false);
        } 
    }

    useEffect(() => {
        const url = new URL(window.location.href);
        hasToken(url);
    });

    const [firstPassword, setFirstPassword] = useState("");
    const [confirmedPassword, setConfirmedPassword] = useState("");
    const [errorMessage, setErrorMessage] = useState("");

    const submitHandler = (event) => {
        event.preventDefault();
        if(firstPassword.length < 6 || confirmedPassword.length < 6){
            setErrorMessage("Minimum 6 character length password!");
            return
        }

        if(firstPassword !== confirmedPassword){
            setErrorMessage("Password didn't matched!");
            return
        }
        
        setErrorMessage("");
        setLoading(true);

        const formData = new FormData();
        formData.append("token", token);
        formData.append("password", confirmedPassword);
        axios
        .post(
            "http://localhost:8080/reset_password",
            formData
            )
        .then(response => {
            console.log(response);
            setErrorMessage(response.data);
            setLoading(false);
            props.history.push("/");
        })
        .catch(err => {
            console.log(err.response.data.message);
            setErrorMessage(err.response.data.message);
            setLoading(false);
        });
    }

    const resetForm = (
        <>
        <h2>New Password</h2>
        <form onSubmit={submitHandler}>
            <p>{errorMessage}</p>
            <input 
                type="password" 
                className="resetPassword_inputField" 
                onChange={(event) => setFirstPassword(event.target.value)} 
                placeholder="Enter new password."
                required
            />

            <input 
                type="password" 
                className="resetPassword_inputField" 
                onChange={(event) => setConfirmedPassword(event.target.value)} 
                placeholder="Retype new password."
                required
            />

            <Button btnType="Success">Reset Password</Button>
        </form>
        </>
        
    );

    return (
        <div className="reset_password">
            {!loading ? resetForm : <Spinner />}
        </div>
        );
}
 
export default ResetPassword;