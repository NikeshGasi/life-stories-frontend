import React, {useState} from 'react';
import axios from "axios";

import "./ResetPassword.css";
import Button from "../../../components/UI/Button/Button";
import { checkValidity } from "../../../shared/utility";
import Spinner from "../../../components/UI/Spinner/Spinner";

const ResetPasswordEmailForm = (props) => {

    const [loading, setLoading] = useState(false);
    const [userEmail, setUserEmail] = useState("");
    const [isValid, setIsValid] = useState(false);
    const [validationMessage, setValidationMessage] = useState("");
    const [touched, setTouched] = useState(false)

    const inputChangedHandler = (event) => {
        let userEmail = event.target.value;
        setIsValid(checkValidity(userEmail, {
            required: true,
            isEmail: true,
        }));
        setUserEmail(userEmail);
        setTouched(true);        
      };

    const submitHandler = (event) => {
        event.preventDefault();
        setLoading(true)

        if(!isValid && touched){
            setValidationMessage( <p>Please provide a valid email address.</p>);
            setLoading(false);
            return;
        }

        const formData = new FormData();
        formData.append("email", userEmail);
        axios
        .post(
            "http://localhost:8080/forgot_password",
            formData
        )
        .then((response) => {
            setLoading(false);
            props.history.push("/reset_message");
        })
        .catch((error) => {
            setValidationMessage(error.response.data.message);
            setLoading(false);
          });
      };

    let spinner = loading && <Spinner />;

    return (
    <div className="reset_password">
    <h2>Reset Password</h2>
    {validationMessage}
    {spinner}
    <form onSubmit={submitHandler}>
        <input 
        type="email" 
        name="email" 
        className="resetPassword_inputField" 
        placeholder="Enter your email."
        onChange={(event) => inputChangedHandler(event)}
        required
        />
        <Button btnType="Success" disabled={loading}>Reset Password</Button>
    </form>
    </div>
    );
}
 
export default ResetPasswordEmailForm;