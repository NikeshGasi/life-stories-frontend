import React from 'react'

const Public = (props) => {
    return ( 
        <>
        <h1>Main Menu</h1>
        <main>{props.children}</main>
        </>
     );
}
 
export default Public;