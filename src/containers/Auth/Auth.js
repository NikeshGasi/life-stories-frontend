import React, { useState } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Redirect } from 'react-router-dom';

import classes from "./Auth.module.css";
import Input from "../../components/UI/Input/Input";
import Button from "../../components/UI/Button/Button";
import { updateObject, checkValidity } from "../../shared/utility";
import avatar from "../../assests/img/img_avatar.png";
import * as actions from "../../store/actions/index";
import Spinner from "../../components/UI/Spinner/Spinner";
//import { loggedInUserRole } from "../../shared/utility";

const Auth = (props) => {
  const [authForm, setAuthForm] = useState({
    username: {
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "Username",
      },
      value: "",
      validation: {
        required: true,
        minLength: 5,
        maxLength: 15
      },
      valid: false,
      touched: false,
      label: "username",
    },
    password: {
      elementType: "input",
      elementConfig: {
        type: "password",
        placeholder: "Password",
      },
      value: "",
      validation: {
        required: true,
        minLength: 5,
        maxLength: 40
      },
      valid: false,
      touched: false,
    },
  });

  /*
  useEffect(() => {
    console.log("auth redirect", props);
    if (props.authRedirectPath !== '/') {
      props.onSetAuthRedirectPath(props.userRole === "ADMIN" ? "/admin" : "/");
    }
  }, []);
  */

  const inputChangedHandler = (event, controlName) => {
    const updatedControls = updateObject(authForm, {
      [controlName]: updateObject(authForm[controlName], {
        value: event.target.value,
        valid: checkValidity(
          event.target.value,
          authForm[controlName].validation
        ),
        touched: true,
      }),
    });
    setAuthForm(updatedControls);
  };

  const submitHandler = async (event) => {
    event.preventDefault();
    await props.onAuth(authForm.username.value, authForm.password.value);
    
    /*
    const jwt = localStorage.getItem("token");
    const isAdmin = jwt ? loggedInUserRole(jwt) : "";
    let redirectRoute = isAdmin === "ADMIN" ? "/admin" : "/";
    console.log("REDIRECT ROUTE: ", redirectRoute);
    props.history.push(redirectRoute);
    */
  };

  const formElementsArray = [];
  for (let key in authForm) {
    formElementsArray.push({
      id: key,
      config: authForm[key],
    });
  }

  let form = formElementsArray.map((formElement) => (
    <Input
      key={formElement.id}
      elementType={formElement.config.elementType}
      elementConfig={formElement.config.elementConfig}
      value={formElement.config.value}
      invalid={!formElement.config.valid}
      shouldValidate={formElement.config.validation}
      touched={formElement.config.touched}
      changed={(event) => inputChangedHandler(event, formElement.id)}
    />
  ));

  if (props.loading) {
    form = <Spinner />;
  }

  let errorMessage = null;

  if (props.error) {
    errorMessage = <p>{props.error}</p>;
  }

  let authRedirect = null;
  if (props.isAuthenticated) {
    authRedirect = <Redirect to={props.authRedirectPath} />;
  }

  return (
    <div className={classes.Auth}>
      {authRedirect}
      <h2>Please Sign in</h2>
      <img src={avatar} alt="Avatar" />
      {errorMessage}
      <form onSubmit={submitHandler}>
        {form}
        <Link to="#"> Forget password? </Link>
        <Button btnType="Success">Sign In</Button>
      </form>
    </div>
  );
};

const mapStateProps = (state) => {
  return {
    userRole: state.auth.userRole,
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.userId !== null,
    authRedirectPath: state.auth.authRedirectPath,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAuth: async (username, password) =>
      dispatch(actions.auth(username, password)),
    onSetAuthRedirectPath: (redirectRoute) =>
      dispatch(actions.setAuthRedirectPath(redirectRoute)),
  };
};

export default connect(mapStateProps, mapDispatchToProps)(Auth);
