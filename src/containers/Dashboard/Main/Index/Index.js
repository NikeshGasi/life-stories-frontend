import React from "react";

import classes from "./Index.module.css";

const index = (props) => {
  return (
    <div className={classes.Index}>
      <h2>Welcome to MyBlog</h2>
    </div>
  );
};

export default index;
