import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

import classes from "./Posts.module.css";
import { updateObject } from "../../../../shared/utility";
import Modal from "../../../../components/UI/Modal/Modal";
import Spinner from "../../../../components/UI/Spinner/Spinner";

const Post = (props) => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [httpError, setHttpError] = useState({ message: "", status: false });
  const [postsState, setPostsState] = useState(false);

  useEffect(() => {
    setLoading(true);
    const url = "http://localhost:8080/posts";
    axios
      .get(url)
      .then((response) => {
        const respondPosts = response.data._embedded.posts;
        const allPosts = respondPosts.map((post, index) => {
          return {
            num: index,
            id: post.id,
            title: post.post_title,
            link: post._links.post.href,
          };
        });
        setPosts(allPosts);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
        const errorMessage = (httpError) => {
          return updateObject(httpError, {
            message: error.response.data.error,
            status: true,
          });
        };
        setHttpError(errorMessage);
        console.log(error.response.data);
      });
  }, [postsState]);

  const httpErrorHandler = () => {
    setHttpError({ message: "", status: false });
    props.history.push("/admin/index");
  };

  const postDeleteHandler = (url) => {
    if (!window.confirm("Delete post?")) {
      return;
    }
    setLoading(true);
    const token = localStorage.getItem("token");
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    axios
      .delete(url, { headers: headers })
      .then((response) => {
        console.log(response);
        setPostsState(!postsState);
      })
      .catch((error) => {
        const errorMessage = (httpError) => {
          return updateObject(httpError, {
            message: error.response.data.message,
            status: true,
          });
        };
        setHttpError(errorMessage);
        console.log(error.response.data);
      });
    setLoading(false);
  };

  let httpErrors = httpError.status ? (
    <Modal show={httpError.message} modalClosed={httpErrorHandler}>
      {httpError.message}
    </Modal>
  ) : null;

  const postList = posts.map((post) => (
    <tr key={post.id}>
      <td>{post.num + 1}</td>
      <td>
        <Link to={"/post/" + post.id}>{post.title}</Link>
      </td>
      <td>
        <Link to={"/admin/post/edit/" + post.id}>Edit</Link>
        <button className="Danger" onClick={() => postDeleteHandler(post.link)}>
          Delete
        </button>
      </td>
    </tr>
  ));

  const postTable = loading ? (
    <Spinner />
  ) : (
    <table>
      <thead>
        <tr>
          <th>S.N</th>
          <th>Title</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>{postList.length !== 0 ? postList : null}</tbody>
    </table>
  );

  return (
    <div className={classes.Post}>
      <h2>Posts</h2>
      {httpErrors}
      <input type="text" placeholder="Search" />
      <hr />
      {postTable}
    </div>
  );
};

export default Post;
