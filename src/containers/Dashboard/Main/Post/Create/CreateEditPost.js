import React, { useState, useEffect } from "react";

import classes from "./CreateEditPost.module.css";
import Input from "../../../../../components/UI/Input/Input";
import Button from "../../../../../components/UI/Button/Button";
import { updateObject, checkValidity } from "../../../../../shared/utility";
import Modal from "../../../../../components/UI/Modal/Modal";
import Spinner from "../../../../../components/UI/Spinner/Spinner";
import http from "../../../../../shared/axios-instance";

const CreatePost = (props) => {
  const editPostId = props.match.params.id;
  const [httpError, setHttpError] = useState({ message: "", status: false });
  const [loading, setLoading] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState("");

  useEffect(() => {
    setLoading(true);
    http
      .get("/categories")
      .then((response) => {
        let cats = response.data._embedded.categories;
        const categories = cats.map((cat) => {
          return {
            displayValue: cat.category_title,
            value: cat._links.category.href,
          };
        });
        const updatedPostForm = (postForm) => {
          return updateObject(postForm, {
            category: updateObject(postForm.category, {
              elementConfig: updateObject(postForm.category.elementConfig, {
                options: categories,
              }),
            }),
          });
        };
        setPostForm(updatedPostForm);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
        const errorMessage = (httpError) => {
          return updateObject(httpError, {
            message: error.response.data.error,
            status: true,
          });
        };
        setHttpError(errorMessage);
        console.log(error.response.data);
      });

    if (editPostId > 0) {
      http
        .get("/posts/" + editPostId)
        .then((response) => {
          // For post's exist category
          http
            .get(response.data._links.categories.href)
            .then((response) => {
              const selectedCategory =
                response.data._embedded.categories[0]._links.category.href;
              setSelectedCategory(selectedCategory);
            })
            .catch((error) => {
              console.log(error);
            });

          const updatedPostForm = (postForm) => {
            return updateObject(postForm, {
              title: updateObject(postForm.title, {
                value: response.data.post_title,
              }),
              content: updateObject(postForm.content, {
                value: response.data.post_content,
              }),
              category: updateObject(postForm.category, {
                value: selectedCategory,
              }),
            });
          };
          setPostForm(updatedPostForm);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [editPostId, selectedCategory]);

  const httpErrorHandler = () => {
    setHttpError({ message: "", status: false });
    props.history.push("/admin/");
  };

  let httpErrors = httpError.status ? (
    <Modal show={httpError.message} modalClosed={httpErrorHandler}>
      {httpError.message}
    </Modal>
  ) : null;

  const [postForm, setPostForm] = useState({
    title: {
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "Title",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
      formDataKey: "post_title",
    },
    content: {
      elementType: "textarea",
      elementConfig: {},
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
      formDataKey: "post_content",
    },
    category: {
      elementType: "select",
      elementConfig: {
        options: [{ value: "empty", displayValue: "Empty" }],
      },
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
      formDataKey: "categories",
    },
  });

  const [formIsValid, setFormIsValid] = useState(false);
  const [formValidationMessage, setFormValidationMessage] = useState();
  const inputChangedHandler = (event, controlName) => {
    const updatedControls = updateObject(postForm, {
      [controlName]: updateObject(postForm[controlName], {
        value: event.target.value,
        valid: checkValidity(
          event.target.value,
          postForm[controlName].validation
        ),
        touched: true,
      }),
    });
    let formIsValid = true;
    let validTitle = updatedControls["title"].value ? true : false;
    formIsValid = validTitle && formIsValid;
    setPostForm(updatedControls);
    setFormIsValid(formIsValid);
  };

  const submitHandler = (event) => {
    event.preventDefault();
    if (!formIsValid) {
      setFormValidationMessage(<p>Title field cannot be empty!</p>);
      return;
    }
    
    setLoading(true);
    let formData = {};
    for (let formControl in postForm) {
      let formDataKey = postForm[formControl].formDataKey;
      if (formControl === "category") {
        formData["categories"] = [postForm[formControl].value];
      } else {
        formData[formDataKey] = postForm[formControl].value;
      }
    }

    if (editPostId > 0) {
      http
        .patch("/posts/" + editPostId, JSON.stringify(formData))
        .then((response) => {
          setLoading(false);
          props.history.push("/admin/posts");
        })
        .catch((error) => {
          setLoading(false);
          const errorMessage = (httpError) => {
            return updateObject(httpError, {
              message: error.response.data.error,
              status: true,
            });
          };
          setHttpError(errorMessage);
          console.log(error.response);
        });
    } else {
      http
        .post("/posts/", JSON.stringify(formData))
        .then((response) => {
          setLoading(false);
          props.history.push("/admin/posts");
        })
        .catch((error) => {
          setLoading(false);
          const errorMessage = (httpError) => {
            return updateObject(httpError, {
              message: error.response.data.error,
              status: true,
            });
          };
          setHttpError(errorMessage);
          console.log(error.response);
        });
    }
  };

  const formElementsArray = [];
  for (let key in postForm) {
    formElementsArray.push({
      id: key,
      config: postForm[key],
    });
  }

  let form = formElementsArray.map((formElement) => {
    return (
      <Input
        key={formElement.id}
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        value={formElement.config.value}
        invalid={!formElement.config.valid}
        shouldValidate={formElement.config.validation}
        touched={formElement.config.touched}
        changed={(event) => inputChangedHandler(event, formElement.id)}
      />
    );
  });

  let btnText = editPostId > 0 ? "Update" : "Create";
  let formHeading = editPostId > 0 ? "Update Post" : "Create Post";

  let postComleteForm = (
    <form onSubmit={submitHandler}>
      {form}
      <input type="file" name="fileToUpload" id="fileToUpload" />
      <Button btnType="Success" disabled={!formIsValid}>
        {btnText}
      </Button>
    </form>
  );
  let post = loading ? <Spinner /> : postComleteForm;

  return (
    <div className={classes.CreatePost}>
      <h2>{formHeading}</h2>
      {formValidationMessage}
      {httpErrors}
      {post}
    </div>
  );
};

export default CreatePost;
