import React, { useState } from "react";

import classes from "./CreateUser.module.css";
import Input from "../../../../../components/UI/Input/Input";
import Button from "../../../../../components/UI/Button/Button";
import { updateObject, checkValidity } from "../../../../../shared/utility";
import Modal from "../../../../../components/UI/Modal/Modal";
import Spinner from "../../../../../components/UI/Spinner/Spinner";
import http from "../../../../../shared/axios-instance";

const CreateUser = (props) => {
  const [httpError, setHttpError] = useState({ message: "", status: false });
  const [loading, setLoading] = useState(false);
  const [formIsValid, setFormIsValid] = useState(false);
  const [formValidationMessage, setFormValidationMessage] = useState();
  const [userPhoto, setUserPhoto] = useState(null);

  const httpErrorHandler = () => {
    setHttpError({ message: "", status: false });
    props.history.push("/admin/");
  };

  let httpErrors = httpError.status ? (
    <Modal show={httpError.message} modalClosed={httpErrorHandler}>
      {httpError.message}
    </Modal>
  ) : null;

  const [userForm, setUserForm] = useState({
    username: {
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "Username",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
      formDataKey: "user_login",
    },
    fname: {
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "Full Name",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
      formDataKey: "user_fname",
    },
    email: {
      elementType: "input",
      elementConfig: {
        type: "email",
        placeholder: "Email",
      },
      value: "",
      validation: {
        required: true,
        isEmail: true,
      },
      valid: false,
      touched: false,
      formDataKey: "user_email",
    },
    password: {
      elementType: "input",
      elementConfig: {
        type: "password",
        placeholder: "Password",
      },
      value: "",
      validation: {
        required: true,
        minLength: 6,
      },
      valid: false,
      touched: false,
      formDataKey: "user_password",
    },
    roles: {
      elementType: "select",
      elementConfig: {
        options: [
          { value: "SUBSCRIBER", displayValue: "Subscriber" },
          { value: "EDITOR", displayValue: "Editor" },
          { value: "ADMIN", displayValue: "Admin" },
        ],
      },
      value: "SUBSCRIBER",
      validation: {},
      valid: true,
      formDataKey: "user_roles",
    },
  });

  const inputChangedHandler = (event, controlName) => {
    const updatedControls = updateObject(userForm, {
      [controlName]: updateObject(userForm[controlName], {
        value: event.target.value,
        valid: checkValidity(
          event.target.value,
          userForm[controlName].validation
        ),
        touched: true,
      }),
    });
    let formIsValid = true;
    for (let inputIdentifier in updatedControls) {
      formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
    }
    setUserForm(updatedControls);
    setFormIsValid(formIsValid);
  };

  const onChangeFileHandler = (event) => {
    let imageFile = event.target.files[0];
    if (!imageFile.name.match(/\.(jpg|jpeg|png|gif)$/)) {
      setFormValidationMessage(<p>Please select valid image.</p>);
      setFormIsValid(false);
    } else {
      setUserPhoto(imageFile);
      setFormValidationMessage(null);
      setFormIsValid(true);
    }
  }
  const [userPhotoUrl, setUserPhotoUrl] = useState("");
  const submitHandler = (event) => {
    event.preventDefault();
    
    if (!formIsValid) {
      setFormValidationMessage(<p>All fields required!</p>);
      return;
    }

    if(userPhoto) {
      const formData = new FormData();
      formData.append("file", userPhoto);

      http
      .post("/s3/uploadFile", formData)
      .then(response => {
        setUserPhotoUrl(response.data);
        console.log(response.data);
        console.log("IMAGE URL: ", userPhotoUrl);
      })
      .catch(error => {
        console.log(error);
      });
    }
    
    // setLoading(true);
    let formData = {};
    for (let formControl in userForm) {
      let formDataKey = userForm[formControl].formDataKey;
      formData[formDataKey] = userForm[formControl].value;
    }

    if(userPhotoUrl) {
      console.log("INSIDE CONDITIONS");
      formData["user_image"] = userPhotoUrl;
    }

    console.log(formData);
    return;

    http
      .post("/consumers/", JSON.stringify(formData))
      .then((response) => {
        setLoading(false);
        props.history.push("/admin/users");
      })
      .catch((error) => {
        setLoading(false);
        const errorMessage = (httpError) => {
          return updateObject(httpError, {
            message: error.response.data.message,
            status: true,
          });
        };
        setHttpError(errorMessage);
      });
  };

  const formElementsArray = [];
  for (let key in userForm) {
    formElementsArray.push({
      id: key,
      config: userForm[key],
    });
  }

  let form = formElementsArray.map((formElement) => {
    return (
      <Input
        key={formElement.id}
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        value={formElement.config.value}
        invalid={!formElement.config.valid}
        shouldValidate={formElement.config.validation}
        touched={formElement.config.touched}
        changed={(event) => inputChangedHandler(event, formElement.id)}
      />
    );
  });

  let postComleteForm = (
    <form onSubmit={submitHandler}>
      {form}

      <input 
        type="file" 
        name="fileToUpload" 
        onChange={(event) => 
        onChangeFileHandler(event)}  
      />
      
      <Button btnType="Success" disabled={!formIsValid}>
        Create User
      </Button>
    </form>
  );
  let post = loading ? <Spinner /> : postComleteForm;

  return (
    <div className={classes.CreatePost}>
      <h2>Create User</h2>
      {formValidationMessage}
      {httpErrors}
      {post}
    </div>
  );
};

export default CreateUser;
