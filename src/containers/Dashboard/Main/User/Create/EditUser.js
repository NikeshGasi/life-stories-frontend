import React, { useEffect, useState } from "react";

import http from "../../../../../shared/axios-instance";
import { updateObject } from "../../../../../shared/utility";
import Modal from "../../../../../components/UI/Modal/Modal";
import Spinner from "../../../../../components/UI/Spinner/Spinner";
import ResetPassword from "../Parts/ResetUserPassword";
import ResetFullname from "../Parts/ResetUserFullname";
import ChangeRole from "../Parts/ChangeUserRole";

const EditUser = (props) => {
  const userId = props.match.params.id;
  const [userData, setUserData] = useState({});
  const [loading, setLoading] = useState(false);
  const [httpError, setHttpError] = useState({ message: "", status: false });

  useEffect(() => {
    http
      .get("/consumers/" + userId)
      .then((response) => {
        setLoading(true);
        const userData = {
          username: response.data.user_login,
          fullname: response.data.user_fname,
          email: response.data.user_email,
          userRole: response.data.user_roles.substr(5),
        };
        setUserData(userData);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(true);
        const errorMessage = (httpError) => {
          return updateObject(httpError, {
            message: error.response.data.error,
            status: true,
          });
        };
        setHttpError(errorMessage);
        console.log(error);
        setLoading(false);
      });
  }, [userId]);

  const httpErrorHandler = () => {
    setHttpError({ message: "", status: false });
    props.history.push("/admin/");
  };

  let httpErrors = httpError.status ? (
    <Modal show={httpError.message} modalClosed={httpErrorHandler}>
      {httpError.message}
    </Modal>
  ) : null;

  let userInfo = (
    <>
      <h2>Edit User</h2>
      <p>Username: {userData.username} </p>
      <p>Email: {userData.email} </p>
      <p>Name: </p>
      {<ResetFullname currentName={userData.fullname} />}
      <p>Role</p>
      {<ChangeRole currentRole={userData.userRole} />}
      <p>Reset Password</p>
      {<ResetPassword currentPassword="password" />}
    </>
  );

  let user = loading ? <Spinner /> : userInfo;

  return (
    <div>
      {httpErrors}
      {user}
    </div>
  );
};

export default EditUser;
