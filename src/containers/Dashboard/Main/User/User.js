import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

import classes from "./User.module.css";
import { updateObject } from "../../../../shared/utility";
import Modal from "../../../../components/UI/Modal/Modal";
import Spinner from "../../../../components/UI/Spinner/Spinner";

const User = (props) => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(false);
  const [httpError, setHttpError] = useState({ message: "", status: false });
  const [userState, setUserState] = useState(false);

  useEffect(() => {
    setLoading(true);
    const url = "http://localhost:8080/consumers";
    const token = localStorage.getItem("token");
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    axios
      .get(url, { headers: headers })
      .then((response) => {
        const respondUsers = response.data._embedded.consumers;
        const allUsers = respondUsers.map((user, index) => {
          return {
            num: index,
            id: user.id,
            username: user.user_login,
            fullname: user.user_fname,
            email: user.user_email,
            role: user.user_roles,
            link: user._links.self.href,
          };
        });
        setUsers(allUsers);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
        const errorMessage = (httpError) => {
          return updateObject(httpError, {
            message: error.response.data.error,
            status: true,
          });
        };
        setHttpError(errorMessage);
        console.log(error);
      });
  }, [userState]);

  const httpErrorHandler = () => {
    setHttpError({ message: "", status: false });
    props.history.push("/admin");
  };

  const userDeleteHandler = (url) => {
    if (!window.confirm("Delete user?")) {
      return;
    }
    setLoading(true);
    const token = localStorage.getItem("token");
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    axios
      .delete(url, { headers: headers })
      .then((response) => {
        setUserState(!userState);
      })
      .catch((error) => {
        const errorMessage = (httpError) => {
          return updateObject(httpError, {
            message: error.response.data.message,
            status: true,
          });
        };
        setHttpError(errorMessage);
        console.log(error.response.data);
      });
    setLoading(false);
  };

  let httpErrors = httpError.status ? (
    <Modal show={httpError.message} modalClosed={httpErrorHandler}>
      {httpError.message}
    </Modal>
  ) : null;

  const userList = users.map((user) => (
    <tr key={user.id}>
      <td>{user.num + 1}</td>
      <td>{user.username}</td>
      <td>
        <Link to={"/user/" + user.id}>{user.fullname}</Link>
      </td>
      <td>{user.email}</td>
      <td>{user.role.substr(5)}</td>
      <td>
        <Link to={"/admin/user/edit/" + user.id}>Edit</Link>
        <button className="Danger" onClick={() => userDeleteHandler(user.link)}>
          Delete
        </button>
      </td>
    </tr>
  ));

  const userTable = loading ? (
    <Spinner />
  ) : (
    <table>
      <thead>
        <tr>
          <th>S.N</th>
          <th>Username</th>
          <th>Full name</th>
          <th>Email</th>
          <th>Role</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>{userList.length !== 0 ? userList : null}</tbody>
    </table>
  );

  return (
    <div className={classes.User}>
      <h2>Users</h2>
      {httpErrors}
      <input type="text" placeholder="Search" />
      <hr />
      {userTable}
    </div>
  );
};

export default User;
