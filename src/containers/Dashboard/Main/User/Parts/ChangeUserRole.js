import React, { useState } from "react";

import Input from "../../../../../components/UI/Input/Input";
import Button from "../../../../../components/UI/Button/Button";
// import http from "../../../../../shared/axios-instance";
import { updateObject, checkValidity } from "../../../../../shared/utility";

const ChangeUserRole = (props) => {
  console.log("USER DATA", props.currentRole);
  const [userRoleOptions, setUserRoleOptions] = useState({
    roles: {
      elementType: "select",
      elementConfig: {
        options: [
          { value: "SUBSCRIBER", displayValue: "Subscriber" },
          { value: "EDITOR", displayValue: "Editor" },
          { value: "ADMIN", displayValue: "Admin" },
        ],
      },
      value: props.currentRole,
      validation: {},
      valid: true,
      formDataKey: "user_roles",
    },
  });

  const [formIsValid, setFormIsValid] = useState(false);
  //   const [formValidationMessage, setFormValidationMessage] = useState();
  const inputChangedHandler = (event, controlName) => {
    const updatedControls = updateObject(userRoleOptions, {
      [controlName]: updateObject(userRoleOptions[controlName], {
        value: event.target.value,
        valid: checkValidity(
          event.target.value,
          userRoleOptions[controlName].validation
        ),
        touched: true,
      }),
    });
    let formIsValid = true;
    for (let inputIdentifier in updatedControls) {
      formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
    }
    setUserRoleOptions(updatedControls);
    setFormIsValid(formIsValid);
  };

  const submitHandler = (event) => {
    event.preventDefault();
    return;
  };

  const formElementsArray = [];
  for (let key in userRoleOptions) {
    formElementsArray.push({
      id: key,
      config: userRoleOptions[key],
    });
  }

  let form = formElementsArray.map((formElement) => {
    return (
      <Input
        key={formElement.id}
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        value={formElement.config.value}
        invalid={!formElement.config.valid}
        shouldValidate={formElement.config.validation}
        touched={formElement.config.touched}
        changed={(event) => inputChangedHandler(event, formElement.id)}
      />
    );
  });

  return (
    <>
      <form onSubmit={submitHandler}>
        {form}
        <Button btnType="Success" disabled={!formIsValid}>
          Change
        </Button>
      </form>
    </>
  );
};

export default ChangeUserRole;
