import React, { useState } from "react";

import Input from "../../../../../components/UI/Input/Input";
import Button from "../../../../../components/UI/Button/Button";
// import http from "../../../../../shared/axios-instance";
import { updateObject, checkValidity } from "../../../../../shared/utility";

const ResetPassword = (props) => {
  console.log("USER DATA", props.currentPassword);
  const [passwordForm, setPasswordFrom] = useState({
    password: {
      elementType: "input",
      elementConfig: {
        type: "password",
        placeholder: "Old Password",
      },
      value: "",
      validation: {
        required: true,
        minLength: 6,
      },
      valid: false,
      touched: false,
    },
    new_password: {
      elementType: "input",
      elementConfig: {
        type: "password",
        placeholder: "New Password",
      },
      value: "",
      validation: {
        required: true,
        minLength: 6,
      },
      valid: false,
      touched: false,
      formDataKey: "user_password",
    },
  });

  const [formIsValid, setFormIsValid] = useState(false);
  //   const [formValidationMessage, setFormValidationMessage] = useState();
  const inputChangedHandler = (event, controlName) => {
    const updatedControls = updateObject(passwordForm, {
      [controlName]: updateObject(passwordForm[controlName], {
        value: event.target.value,
        valid: checkValidity(
          event.target.value,
          passwordForm[controlName].validation
        ),
        touched: true,
      }),
    });
    let formIsValid = true;
    for (let inputIdentifier in updatedControls) {
      formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
    }
    setPasswordFrom(updatedControls);
    setFormIsValid(formIsValid);
  };

  const submitHandler = (event) => {
    event.preventDefault();
    return;
  };

  const formElementsArray = [];
  for (let key in passwordForm) {
    formElementsArray.push({
      id: key,
      config: passwordForm[key],
    });
  }

  let form = formElementsArray.map((formElement) => {
    return (
      <Input
        key={formElement.id}
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        value={formElement.config.value}
        invalid={!formElement.config.valid}
        shouldValidate={formElement.config.validation}
        touched={formElement.config.touched}
        changed={(event) => inputChangedHandler(event, formElement.id)}
      />
    );
  });

  return (
    <>
      <form onSubmit={submitHandler}>
        {form}
        <Button btnType="Success" disabled={!formIsValid}>
          Reset Password
        </Button>
      </form>
    </>
  );
};

export default ResetPassword;
