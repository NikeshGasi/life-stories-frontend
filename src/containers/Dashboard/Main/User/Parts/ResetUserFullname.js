import React, { useState } from "react";

import Input from "../../../../../components/UI/Input/Input";
import Button from "../../../../../components/UI/Button/Button";
// import http from "../../../../../shared/axios-instance";
import { updateObject, checkValidity } from "../../../../../shared/utility";

const ResetFullname = (props) => {
  console.log("USER DATA", props.currentName);
  const [fullnameForm, setFullnameForm] = useState({
    fullname: {
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "Full name",
      },
      value: props.currentName,
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
      formDataKey: "user_fname",
    },
  });

  const [formIsValid, setFormIsValid] = useState(false);
  //   const [formValidationMessage, setFormValidationMessage] = useState();
  const inputChangedHandler = (event, controlName) => {
    const updatedControls = updateObject(fullnameForm, {
      [controlName]: updateObject(fullnameForm[controlName], {
        value: event.target.value,
        valid: checkValidity(
          event.target.value,
          fullnameForm[controlName].validation
        ),
        touched: true,
      }),
    });
    let formIsValid = true;
    for (let inputIdentifier in updatedControls) {
      formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
    }
    setFullnameForm(updatedControls);
    setFormIsValid(formIsValid);
  };

  const submitHandler = (event) => {
    event.preventDefault();
    return;
  };

  const formElementsArray = [];
  for (let key in fullnameForm) {
    formElementsArray.push({
      id: key,
      config: fullnameForm[key],
    });
  }

  let form = formElementsArray.map((formElement) => {
    return (
      <Input
        key={formElement.id}
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        value={formElement.config.value}
        invalid={!formElement.config.valid}
        shouldValidate={formElement.config.validation}
        touched={formElement.config.touched}
        changed={(event) => inputChangedHandler(event, formElement.id)}
      />
    );
  });

  return (
    <>
      <form onSubmit={submitHandler}>
        {form}
        <Button btnType="Success" disabled={!formIsValid}>
          Change
        </Button>
      </form>
    </>
  );
};

export default ResetFullname;
