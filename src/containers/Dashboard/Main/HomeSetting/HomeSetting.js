import React from "react";

import Spinner from "../../../../components/UI/Spinner/Spinner";

const homeSetting = () => {
  return (
    <div className="homeCustomization">
      <h2>Home Customization</h2>
      <Spinner />
    </div>
  );
};

export default homeSetting;
