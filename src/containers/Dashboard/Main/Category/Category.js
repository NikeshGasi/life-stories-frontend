import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

import classes from "./Category.module.css";
import { updateObject } from "../../../../shared/utility";
import Modal from "../../../../components/UI/Modal/Modal";
import Spinner from "../../../../components/UI/Spinner/Spinner";

const Category = (props) => {
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(false);
  const [httpError, setHttpError] = useState({ message: "", status: false });
  const [categoryState, setCategoryState] = useState(false);

  useEffect(() => {
    setLoading(true);
    const url = "http://localhost:8080/categories";
    const token = localStorage.getItem("token");
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    axios
      .get(url, { headers: headers })
      .then((response) => {
        const respondCategories = response.data._embedded.categories;
        const allCategories = respondCategories.map((category, index) => {
          return {
            num: index,
            id: category.id,
            name: category.category_title,
            link: category._links.self.href,
          };
        });
        setCategories(allCategories);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
        const errorMessage = (httpError) => {
          return updateObject(httpError, {
            message: error.response.data.error,
            status: true,
          });
        };
        setHttpError(errorMessage);
        console.log(error);
      });
  }, [categoryState]);

  const httpErrorHandler = () => {
    setHttpError({ message: "", status: false });
    props.history.push("/admin/index");
  };

  const categoryDeleteHandler = (url) => {
    if (!window.confirm("Delete user?")) {
      return;
    }
    setLoading(true);
    const token = localStorage.getItem("token");
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    axios
      .delete(url, { headers: headers })
      .then((response) => {
        console.log("inside response: ", categories);
        setCategoryState(!categoryState);
      })
      .catch((error) => {
        const errorMessage = (httpError) => {
          return updateObject(httpError, {
            message: error.response.data.message,
            status: true,
          });
        };
        setHttpError(errorMessage);
        console.log(error.response.data);
      });
    console.log("outside axiso: ", categories);
    setLoading(false);
  };

  let httpErrors = httpError.status ? (
    <Modal show={httpError.message} modalClosed={httpErrorHandler}>
      {httpError.message}
    </Modal>
  ) : null;

  const categoryList = categories.map((category) => (
    <tr key={category.id}>
      <td>{category.num + 1}</td>
      <td>
        <Link to={"/category/" + category.id}>{category.name}</Link>
      </td>
      <td>
        <Link to={"/category/edit/" + category.id}>Edit</Link>
        <button
          className="Danger"
          onClick={() => categoryDeleteHandler(category.link)}
        >
          Delete
        </button>
      </td>
    </tr>
  ));

  const CategoryTable = loading ? (
    <Spinner />
  ) : (
    <table>
      <thead>
        <tr>
          <th>S.N</th>
          <th>Name</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>{categoryList.length !== 0 ? categoryList : null}</tbody>
    </table>
  );

  return (
    <div className={classes.Category}>
      <h2>Category</h2>
      {httpErrors}
      <input type="text" placeholder="Search" /> <button>Search</button>
      <hr />
      {CategoryTable}
    </div>
  );
};

export default Category;
