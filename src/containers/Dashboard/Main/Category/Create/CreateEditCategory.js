import React, { Fragment } from "react";

import Spinner from "../../../../../components/UI/Spinner/Spinner";

const CreateEditCategory = (props) => {
  return (
    <Fragment>
      <h2>Create Category</h2>
      <Spinner />
    </Fragment>
  );
};

export default CreateEditCategory;
