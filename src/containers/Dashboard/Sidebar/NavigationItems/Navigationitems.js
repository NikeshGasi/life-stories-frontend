import React from "react";

import NavigationItem from "./NavigationItem/NavigationItem";
const NavgationItems = (props) => {
  return (
    <div className="nav-links">
      <NavigationItem link="/admin">Dashboard</NavigationItem>
      <NavigationItem link="/admin/posts">Posts</NavigationItem>
      <NavigationItem link="/admin/posts">All posts</NavigationItem>
      <NavigationItem link="/admin/new-post">Create post</NavigationItem>
      <NavigationItem link="/admin/users">Users</NavigationItem>
      <NavigationItem link="/admin/users">All users</NavigationItem>
      <NavigationItem link="/admin/new-user">Create user</NavigationItem>
      <NavigationItem link="/admin/category">Category</NavigationItem>
      <NavigationItem link="/admin/category">All category</NavigationItem>
      <NavigationItem link="/admin/new-category">
        Create category
      </NavigationItem>
      <NavigationItem link="/admin/homeSetting">Home Setting</NavigationItem>
    </div>
  );
};

export default NavgationItems;
