import React from "react";
import { connect } from "react-redux";

import classes from "./Sidebar.module.css";
import NavigationItems from "./NavigationItems/Navigationitems";
import * as actions from "../../../store/actions/index";
import avatar from "../../../assests/img/user.png";

const Sidebar = (props) => {
  return (
    <div className={classes.Sidenav}>
      <h2>{props.username}</h2>
      <img src={avatar} alt="Avatar" />
      <button
        onClick={() => {
          props.onLogout();
          props.history.push("/auth");
        }}
      >
        Logout
      </button>
      <NavigationItems />
    </div>
  );
};

const mapStateToProps = state => {
  return {
    username: state.auth.username,
    userId: state.auth.userId,
    userRole: state.auth.userRole
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onLogout: () => dispatch(actions.logout()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
