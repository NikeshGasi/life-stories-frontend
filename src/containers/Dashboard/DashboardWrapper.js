import React, { Fragment } from "react";

import classes from "./Dashboard.module.css";
import Sidebar from "./Sidebar/Sidebar";

const Dashboard = (props) => {
  return (
    <Fragment>
      <Sidebar {...props} />
      <main className={classes.Main}>{props.children}</main>
    </Fragment>
  );
};

export default Dashboard;
