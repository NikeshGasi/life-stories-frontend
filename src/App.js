import React from "react";
import { Route, withRouter, Switch, BrowserRouter, Redirect } from "react-router-dom";

import AuthRoute from "./router/AuthRoute";
import PrivateRoute from "./router/PrivateRoute";
import "./App.css";
import Auth from "./containers/Auth/Auth";
import Homepage from "./hoc/Layout/Homepage/Homepage";
import Posts from "./containers/Dashboard/Main/Post/Posts";
import CreateEditPost from "./containers/Dashboard/Main/Post/Create/CreateEditPost";
import Users from "./containers/Dashboard/Main/User/User";
import CreateUser from "./containers/Dashboard/Main/User/Create/CreateUser";
import EditUser from "./containers/Dashboard/Main/User/Create/EditUser";
import Category from "./containers/Dashboard/Main/Category/Category";
import CreateEditCategory from "./containers/Dashboard/Main/Category/Create/CreateEditCategory";
import HomeSetting from "./containers/Dashboard/Main/HomeSetting/HomeSetting";
import Index from "./containers/Dashboard/Main/Index/Index";
import DashboardLayout from "./containers/Dashboard/DashboardWrapper";
import RestPasswordEmailForm from "./containers/Public/ResetPassword/ResetPasswordEmailForm";
import ResetPassword from "./containers/Public/ResetPassword/ResetPasswordForm";
import ResetMessage from "./containers/Public/ResetPassword/ResetMessage";
import { loggedInUserRole } from "./shared/utility";
// import PublicLayout from "./containers/Public/PublicWrapper";
// import PublicRoute from "./router/PublicRoute";

// const Posts = React.lazy(() => import("./containers/Dashboard/Main/Post/Posts"));

const App = (props) => {
  let currentPath = props.history.location.pathname;

  const RedirectToMain = _ => {
    return (
        <Redirect to="/" />
    );
  }
  
  const jwt = localStorage.getItem("token");
  const isAdmin = jwt ? loggedInUserRole(jwt) : "";

  let dashboardRoutes = (
    <DashboardLayout {...props}>
      <PrivateRoute path="/admin/posts" exact component={Posts} />
      <PrivateRoute path="/admin/new-post" exact component={CreateEditPost} />
      <PrivateRoute
        path="/admin/post/edit/:id"
        exact
        component={CreateEditPost}
      />
      <PrivateRoute path="/admin/users" exact component={Users} />
      <PrivateRoute path="/admin/new-user" exact component={CreateUser} />
      <PrivateRoute path="/admin/user/edit/:id" exact component={EditUser} />
      <PrivateRoute path="/admin/category" exact component={Category} />
      <PrivateRoute
        path="/admin/new-category"
        exact
        component={CreateEditCategory}
      />
      <PrivateRoute path="/admin/homeSetting" exact component={HomeSetting} />
      <PrivateRoute path="/admin" exact component={Index} />
    </DashboardLayout>
  );

  const authRoute = (<AuthRoute path="/auth" exact component={Auth} {...props} />);

  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Homepage} />
        <Route path="/reset_password_email_form" exact component={RestPasswordEmailForm} />
        <Route path="/reset_password" exact component={ResetPassword} />
        <Route path="/reset_message" exact component={ResetMessage} />
        { !jwt && authRoute }
        {isAdmin === "ADMIN" && currentPath.includes("/admin") ? dashboardRoutes : ""}
        {/* this route will catch any route that wasnt matched in previous routes */}
        <Route component={RedirectToMain}/> 
      </Switch>
    </BrowserRouter>
  );
};

export default withRouter(App);
